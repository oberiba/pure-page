## Page 1 - html / css

## Colors:
* Background: 			#fff
* Background dark:  		#ecf5fe
* Background light:  		#ECF5FE
* Light blue: 			#5ecfda
* Dark blue: 			#121921
* Font light: 			#fff
* Font semi-light: 		#7f8497
* Font dark: 			#212529
* Font blue:			#007bff
* Font semi: 			#4b5d73
* Font mid: 			#6987abs
* Menu font: 			rgba(0,0,0,.5)

## Font: 
* font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
* font-size: 1rem;
* font-weight: 400;
* line-height: 1.5;

## Content
### DIGITAL MARKETING
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce facilisis neque ac urna lobortis, id ornare sem laoreet

Why choose us
We ensure perfect quality Digital Services for you

Landing Page
Efficiently unleash cross-media information without.

24/7 Support
Efficiently unleash cross-media information without.

Fully Customizable
Efficiently unleash cross-media information without.

Multi-Purpose
Efficiently unleash cross-media information without.
___

About the JOIIND.
High-Performance And Easy To Use Landing Page Theme. The Theme Is Developed Focusing On The Attractive Designs And Easy Customizable Features.

Awesome
modern design
Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.

Supereasy
to customize
Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per.

Responsive
layout
Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per.
___

Gallery & Works

Let Everyone Know Why
We are the Best
___
Our JOIIND. Team
Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per.

MANAGER
John Doe
Sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt est etiam.
___

Contact Us
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,

795 South Park Avenue, 
Melbourne, Australia

8 800 567.890.11 - Central Office 
8 800 567.890.12 - Fax

support@example.com
___

Logo  Multipurpose Startup
Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.

Userful Links

Home
Services
About
Works
Team
Contact

Contact with us
  8 800 567.890.11

  support@example.com